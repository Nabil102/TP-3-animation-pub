TP-3-animation-pub
Scénario de l'animation:

Fond(mode nuit) avec un hibou sur un nuage animé accompagné d'un son.
Le hibou se déplace vers le toit du coffee shop.
La musique change de rythme puis trois cartons tombent et se stabilisent sur le sol.
Le building de la compagnie apparaît.
Une employée de la compagnie entre dans la scène de la gauche et fait signe de sa main pour préparer la rentrée d'un avion qui tire le slogan de la compagnie. Fin de l'animation.
Équipe : Meriam Ourri et Nabil Bouazzaoui